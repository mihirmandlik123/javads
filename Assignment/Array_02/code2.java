/*
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size of an array");
		int n=Integer.parseInt(br.readLine());

		int arr[]=new int[n];
		int even=0,odd=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				even++;
			else
				odd++;
		}
		System.out.println("Number of Even Numbers: "+even);
		System.out.println("Number of Odd Numbers: "+odd);
	}
}
