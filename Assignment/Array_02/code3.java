/*
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size of an array");
		int n=Integer.parseInt(br.readLine());

		int arr[]=new int[n];
		int even=0,odd=0;
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]%2==0)
				even+=arr[i];
			else
				odd+=arr[i];
		}

		System.out.println("Odd Numbers sum = "+odd);
		System.out.println("Even Numbers sum = "+even);
	}
}
