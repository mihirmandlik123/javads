/*
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size of an array");
		int n=Integer.parseInt(br.readLine());

		int arr[]=new int[n];
	
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter Element to search: ");
		int num=Integer.parseInt(br.readLine());
		int flag=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==num){
				System.out.println("Element Found at inndex: "+i);
				flag=1;
				break;
			}
		
		}
		if(flag==0)
			System.out.println("Element not found in the array");
	}
}
