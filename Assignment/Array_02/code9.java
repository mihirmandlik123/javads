/*
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first array size: ");
		int n1=Integer.parseInt(br.readLine());
		System.out.println("Enter the second array size: ");
		int n2=Integer.parseInt(br.readLine());
		int arr1[]=new int[n1];
		int arr2[]=new int[n2];
		System.out.println("Enter the values in array 1: ");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter the values in array 2: ");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		int arr3[]=new int[n1+n2];
		int i=0;
		for( ;i<n1;i++){
			arr3[i]=arr1[i];
		}
		for(int j=0;j<n2;j++){
			arr3[i]=arr2[j];
			i++;
		}
		for(int k=0;k<n1+n2;k++){
			System.out.print(arr3[k]+"\t");
		}

	}
}

