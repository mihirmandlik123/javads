/*
1 2 9
4 25 6
49 8 81
*/

class Demo{
	public static void main(String[] m){
		int a=1;
		for(int i=1;i<=3;i++){
			for(int j=1;j<=3;j++){
				if(j%2==0)
					System.out.print(a++ +"\t");
				else{
					System.out.print(a*a +"\t");
					a++;
				}
			}

			System.out.println();
		}
	}
}
