/*
A b C d
E f G h
I j K l
M n O p
*/

class Demo{
	public static void main(String[] m){
		char a=65;
		char b=97;
		for(int i=1;i<=4;i++){
			for(int j=1;j<=4;j++){
				if(j%2==0)
					System.out.print(b +"\t");
				else
					System.out.print(a +"\t"); 
				a++;
				b++;
			}

			System.out.println();
		}
	}
}
