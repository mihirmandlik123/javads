import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr1= new InputStreamReader(System.in);
		InputStreamReader isr2= new InputStreamReader(System.in);

		BufferedReader br1= new BufferedReader(isr1);		
		BufferedReader br2= new BufferedReader(isr2);

		System.out.println("Enter Society1 Name: ");
	       	String name=br1.readLine();
		System.out.println(name);

		br1.close();
		
		System.out.println("Enter Society2 Name: ");
	       	String name1=br2.readLine();
		System.out.println(name1);
			
	}
}
