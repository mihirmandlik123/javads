/*
D4 C3 B2 A1
A1 B2 C3 D4
D4 C3 B2 A1
A1 B2 C3 D4
*/
import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows ");
		int row=Integer.parseInt(br.readLine());
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print((char)(64+row-j+1)+""+(row-j+1)+"\t");
				}
				else{
					System.out.print((char)(64+j)+""+j+"\t");
				}
			}
			System.out.println();
		}

	}
}
