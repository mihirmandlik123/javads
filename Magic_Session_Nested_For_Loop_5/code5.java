/*
0
1 1
2 3 5
8 13 21 34
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows ");
		int row=Integer.parseInt(br.readLine());
		int sum=0;
		int num1=0;
		int num2=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=i;j++){
				System.out.print(sum+"\t");
				num1=num2;
				num2=sum;
				sum=num1+num2;
			}
			System.out.println();
		}

	}
}
