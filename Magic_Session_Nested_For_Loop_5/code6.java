/*
Input: a p
Output: The difference between a and p is 15
*/

import java.io.*;
class Demo{
	public static void main(String[] args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the first character ");
		char ch1=(char)br.read();
		br.skip(1);
		System.out.println("Enter the second character ");
		char ch2=(char)br.read();

		if(ch1==ch2){
			System.out.println("Both are equal");
		}
		else if(ch1<ch2){
			System.out.println("The diffrence between "+ch1+""+ch2+" is "+(ch2-ch1));
		}
		else{
			System.out.println("The diffrence between "+ch1+""+ch2+" is "+(ch1-ch2));
		}

	}
}
